# Используем базовый образ с Python 3.8
FROM python:3.8-slim

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Устанавливаем системные зависимости для psycopg2
RUN apt-get update && apt-get install -y libpq-dev gcc

# Копируем файл зависимостей в контейнер
COPY requirements.txt requirements.txt

# Устанавливаем зависимости
RUN pip install --no-cache-dir -r requirements.txt

# Копируем всё содержимое текущей директории в рабочую директорию в контейнере
COPY . .

# Устанавливаем переменные окружения для Flask
ENV FLASK_APP=app
ENV FLASK_ENV=development

# Выполняем миграции базы данных
RUN flask db upgrade

# Заполняем базу данных начальными данными
RUN python seed.py

# Указываем команду по умолчанию для запуска приложения с использованием встроенного сервера Flask
CMD ["flask", "run", "--host=0.0.0.0", "--port=5000"]

